import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

const IMAGES_API = 'http://localhost:8080/api/image/';

@Injectable({
  providedIn: 'root'
})
export class ImagesUploadService {

  constructor(private http: HttpClient) {
  }



  uploadImagesToPost(file: File, postId: number): Observable<any> {
    const uploadData: FormData = new FormData();
    uploadData.append('file', file);

    return this.http.post(IMAGES_API + postId + '/upload', uploadData);
  }

  getFiles(): Observable<any> {
    return this.http.get(`${IMAGES_API}/files`);
  }
}
